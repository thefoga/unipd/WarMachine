# Change Log
All notable changes to this project will be documented in this file.

## 0.0.2 - 2017-03-21

### Changed
- folder structure
- np <-> pdq

## 0.0.1 - 2017-03-13

### Added
- raw folder structure
