<div align="center">
<h1>WarMachine | IronWorks</h1>
<em>Costruire software robusto</em></br></br>

<a href="https://travis-ci.com/sirfoga/WarMachine"><img src="https://travis-ci.com/sirfoga/WarMachine.svg?token=2ioPzyypRgPWTrC2NfJK&branch=master"></a>
<a href="http://unlicense.org/"><img src="https://img.shields.io/badge/license-Unlicense-blue.svg"></a>
</div>

Currently working for `RQ` deadline (09 July, 17:00):

- [Glossario (v3.0.0)](/rq/esterni/glossario/Glossario_v3.0.0.tex)
- [Analisi dei requisiti (v3.0.0)](/rq/esterni/analisi-dei-requisiti/Analisi_Dei_Requisiti_v3.0.0.tex)
- [Piano di progetto (v3.0.0)](/rq/esterni/piano-di-progetto/Piano_Di_Progetto_v3.0.0.tex)
- [Piano di qualifica (v3.0.0)](/rq/esterni/piano-di-qualifica/Piano_Di_Qualifica_v3.0.0.tex)
- [Manuale sviluppatore (v1.0.0)](/rq/esterni/manuali/manuale-sviluppatore/Manuale_Sviluppatore_v1.0.0.tex)
- [Manuale utente (v1.0.0)](/rq/esterni/manuali/manuale-utente/Manuale_Utente_v1.0.0.tex)
- [Lettera di presentazione (v3.0.0)](/rq/esterni/lettera-di-presentazione/Lettera_Di_Presentazione.tex)
- [Norme di progetto (v3.0.0)](/rq/interni/norme-di-progetto/Norme_Di_Progetto_v3.0.0.tex)

Code source [here](https://github.com/sirfoga/IronWorks)


## Feedback
Suggestions and improvements [welcome](https://github.com/sirfoga/WarMachine/issues)!


## Authors
| [izanetti](https://github.com/izanetti "Follow @izanetti on Github") | [elezanon](https://github.com/elezanon "Follow @elezanon on Github") | [![Bragaz](https://avatars1.githubusercontent.com/u/26248202?s=128&v=4)](https://github.com/Bragaz "Follow @Bragaz Github") | [![acoletti-33100](https://avatars1.githubusercontent.com/u/32634632?s=128&v=4)](https://github.com/acoletti-33100 "Follow @acoletti-33100 on Github") | [![ShiroAimai](https://avatars0.githubusercontent.com/u/33904974?s=128&v=4)](https://github.com/ShiroAimai "Follow @ShiroAimai on Github") | [riccardobek](https://github.com/riccardobek "Follow @riccardobek on Github") | [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|---|---|---|---|---|---|
| [Ilenia Zanetti](https://github.com/izanetti) | [Elena Zanon](https://github.com/elezanon) | [Leonardo Bragagnolo](https://github.com/Bragaz) | [Andrea Coletti](https://github.com/acoletti-33100) | [Nicola Cisternino](https://github.com/ShiroAimai) | [Riccardo Bernucci](https://github.com/riccardobek) | [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[Unlicense](https://unlicense.org/)
