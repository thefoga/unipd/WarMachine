# !/usr/bin/python3
# coding: utf-8


def stretch(val, ratio):
    if val < 0 and ratio < 1:
        ratio = (1 + (1 - ratio))

    return val * ratio


def filter_data(dates, delta_max=2):
    out = [dates[0]]
    for i, date in enumerate(dates[1:]):
        if i % delta_max == 0:
            out.append(date)

    if len(out) < 3:
        return dates
    return out
