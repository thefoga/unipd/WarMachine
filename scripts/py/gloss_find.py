# !/usr/bin/python3
# coding: utf-8

import os
import re
from collections import Counter

from core.cmd import join_paths
from core.files import get_content_of_file
from core.latex import write_glossary, get_tex_includes
from glossary.data import INPUT_FILES
from glossary.misc import parse_args, create_args, print_words, any_case_regex
from glossary.utils import lst_to_glossary, find_glossary_words_folder, \
    parse_glossary_file, find_glossary_words_file, \
    get_all_written_glossary_words


def generate_glossary():
    """
    :return: void
        Parses files from input folder, then writes .tex output
    """

    folder, _ = parse_args(create_args())
    output_file = os.path.join(folder, "output.tex")
    glossary = find_glossary_words_folder(folder)
    write_glossary(
        sorted(set(glossary)),
        output_file
    )
    print("Output .tex is", output_file)


def check_doc(path, in_glossary):
    """
    :param path: str
        Path to tex file
    :param in_glossary: Counter
        Words in glossary file
    :return: void
        Reports document
    """

    included_files = get_tex_includes(path, os.path.dirname(path))

    # retrieve glossary words
    in_text = []  # all words in included files
    full_words = []
    for fil in included_files:
        glossary_words, all_words = find_glossary_words_file(fil)
        in_text += glossary_words
        full_words += all_words
    in_text = Counter(lst_to_glossary(in_text))

    first_occurrences = {}
    for word in in_text:
        for fil in included_files:
            text = get_content_of_file(fil)  # read file
            word_in_text = re.search(any_case_regex(word), text) is not None
            if word not in first_occurrences and word_in_text:
                first_occurrences[word] = os.path.basename(fil)

    print_words(in_glossary, in_text, "Glossary report for " + path,
                first_occurrences)

    if "verbali" in path:
        print(path, "api" in full_words)
    return in_text, full_words


def print_missing_extra(in_glossary, in_all, all_words):
    missing = [
        word for word, occurrence in sorted(in_all.items(), key=lambda x: x[0])
        if word not in in_glossary
    ]
    extra = [
        word for word, occurrence in
        sorted(in_glossary.items(), key=lambda x: x[0])
        if word not in in_all
    ]

    if missing:
        print("\nMissing glossary words:")
        for word in missing:
            print("\t-", word)

    if extra:
        print("\nUnused glossary words (extra words)", len(extra), "words:")
        for word in extra:
            print("\t-", word)

        really_unused = [
            word for word in extra if word not in all_words
        ]
        print("\nReally unused glossary words (not marked \\gl, even if they "
              "are in glossary)", len(really_unused), "words:")
        for word in really_unused:
            print("\t-", word)


def check():
    """
    :return: void
        Parses files from input folder, then outputs words that are not in
        glossary
    """

    folder, glossary_file = parse_args(create_args())
    files = [
        join_paths(folder, fil) for fil in INPUT_FILES
    ]
    in_glossary = parse_glossary_file(glossary_file)  # in glossary
    in_all = get_all_written_glossary_words(files)
    all_words = []

    for fil in files:
        glossary_words, words = check_doc(fil, in_glossary)
        all_words += words

    all_words = [
        re.sub(r'[^\w]', '', word.lower()).strip() for word in all_words
    ]
    all_words = Counter(lst_to_glossary(all_words))
    print_missing_extra(in_glossary, in_all, all_words)


if __name__ == "__main__":
    check()
