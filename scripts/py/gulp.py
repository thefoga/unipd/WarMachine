# !/usr/bin/python3
# coding: utf-8

import argparse
import os

import numpy as np

from core.cmd import join_paths
from core.files import get_content_of_latex_file
from core.latex import get_tex_includes
from core.pretty_table import pretty_format_table
from readability.data import INPUT_FILES
from readability.readability import gulpease

LEVELS = [
    (40, "advanced"),
    (60, "medium"),
    (80, "elementary"),
    (float("inf"), "easy peasy")
]


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage="-f <folder to scan> -g <glossary to use> -o <output file>"
              "-h for full usage")
    parser.add_argument("-f", dest="fol",
                        help="folder to scan", type=str, required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    folder = args.fol
    assert os.path.exists(folder)

    return folder


def get_gulp_grading(gulp):
    """
    :param gulp: int
        Gulpease index
    :return: str
        Level of text
    """

    for (key, val) in LEVELS:
        if gulp < key:
            return val


def gulp_file(path):
    """
    :param path: str
        Path of .tex file to read
    :return: tuple (float, str, int)
        Gulpease index and grade, lenght of text
    """

    text = get_content_of_latex_file(path)
    gulp = round(gulpease(text))
    return gulp, get_gulp_grading(gulp), len(text)


def gulp_doc(path):
    """
    :param path: str
        Path of folder where to find tex files
    :return: void
        Prints readability index of .tex files found in folder
    """

    included_files = get_tex_includes(path, os.path.dirname(path))
    data = []
    gulp_weights = []  # single file gulp index and length
    for fil in included_files:
        gulp, msg, length = gulp_file(fil)
        file_name = fil.replace(path, "")
        data.append([file_name, gulp, msg])
        gulp_weights.append((gulp, length))

    data = sorted(data, key=lambda x: x[1])
    entire_gulp = np.average(
        [x[0] for x in gulp_weights],  # gulp of doc
        weights=[x[1] for x in gulp_weights]  # length of doc
    )

    print("\nGulpease report of", path)
    print("Average Gulpease index of document:", round(entire_gulp))
    print(pretty_format_table(["file", "gulpease", "grade"], data))


def cli():
    """
    :return: void
        Main routine
    """

    folder = parse_args(create_args())
    files = [
        join_paths(folder, fil) for fil in INPUT_FILES
    ]

    for fil in files:
        gulp_doc(fil)


if __name__ == "__main__":
    cli()
