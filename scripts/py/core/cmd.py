import os
import subprocess

TEX_TEXT_CMD = "detex -n"


def call_command(cmd):
    """
    :param cmd: str
        Shell command to call
    :return: str
        Output of command
    """

    return subprocess.check_output(cmd.split(" "))


def detex(path):
    """
    :param path: str
        Path of file to detex
    :return: str
        Content of file
    """

    full_command = TEX_TEXT_CMD + " " + path
    return call_command(full_command)


def join_paths(*paths):
    """
    :param paths: tuple of str
        String to join
    :return: str
        Full path
    """

    full_path = []
    for p in paths:
        full_path += p.split(os.sep)
    full_path = os.path.join(*full_path)

    if paths[0].startswith(os.sep):
        full_path = "/" + full_path
    return full_path
