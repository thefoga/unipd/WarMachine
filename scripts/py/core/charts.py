# !/usr/bin/python3
# coding: utf-8

from matplotlib import pyplot as plt, transforms as mtrans

PLOT_PADDING = 0.05
PLOT_PADDED = 1 + PLOT_PADDING


class SubPlotter:
    """ Plots multiple subplots """

    def __init__(self, sub_rows, sub_cols, max_lines_per_plot):
        """
        :param y_ticks: [] of str
            List of ticks to show in y-axis
        :param max_lines_per_plot: int
            Max number of lines per subplot
        """

        self.fig, self.axis = plt.subplots(
            nrows=sub_rows, ncols=sub_cols, sharey=True
        )
        self.level_colors = \
            ["green", "yellow", "yellow", "red", "red", "red", "red"]

        self.sub_rows = sub_rows
        self.sub_cols = sub_cols
        self.max_lines_per_plot = max_lines_per_plot
        self.current_line = 0
        self.current_subrow = 0  # x, y coord for current subplot
        self.current_subcol = 0
        self.padding, self.y_min, self.y_max = None, None, None

    def setup(self, x_ticks, y_ticks):
        """
        :param xlabels: [] of str
            List of labels of x-axis
        :param ticks: [] of str
            List of ticks to show in y-axis
        :return: void
            Setup chart canvas
        """

        self.fig.subplots_adjust(wspace=0.1, hspace=0.6)

        plt.yticks(y_ticks["range"], y_ticks["ticks"])

        # x axis
        delta_y = (y_ticks["range"][-1] - y_ticks["range"][0])
        self.padding = PLOT_PADDING * delta_y
        self.y_min, self.y_max = y_ticks["range"][0] - self.padding, \
                                 y_ticks["range"][-1] + self.padding
        y_boundaries = [self.y_min, self.y_max]
        for i, axis in enumerate(self.fig.axes):
            plt.ylim(y_boundaries)
            plt.sca(axis)
            plt.xticks(rotation=-45)
            offset = mtrans.ScaledTranslation(0.4, 0, self.fig.dpi_scale_trans)
            for label in axis.xaxis.get_majorticklabels():
                label.set_transform(label.get_transform() + offset)

        if x_ticks:
            # first plot
            self.fig.axes[0]. \
                set_xticks(x_ticks["range"][:self.max_lines_per_plot - 1])
            self.fig.axes[0]. \
                set_xticklabels(x_ticks["ticks"][:self.max_lines_per_plot - 1])

            # second plot
            self.fig.axes[1]. \
                set_xticks(x_ticks["range"][self.max_lines_per_plot - 1:])
            self.fig.axes[1]. \
                set_xticklabels(x_ticks["ticks"][self.max_lines_per_plot - 1:])

    def plot_background(self, colors, alpha=0.2):
        """
        :return: void
            Colors plot canvas according to tick level. Specify colors.
        """

        y_min = self.y_min + self.padding * 0.5  # stretch(min([axis.get_ylim()[0] for axis in self.fig.axes]), 0.8)
        y_max = self.y_max - self.padding * 0.5  # stretch(max([axis.get_ylim()[1] for axis in self.fig.axes]), 1.2)

        for axis in self.fig.axes:
            x_min, x_max = axis.get_xlim()

            if axis._visible:  # paint only if necessary
                axis.fill_between(
                    [x_min, x_max], y_min, colors[0]["range"],
                    facecolor=colors[0]["color"], alpha=alpha
                )  # start
                print("Colored", colors[0]["color"], "(alpha =", alpha,
                      "from (", x_min, ",", y_min, ") to (",
                      x_max, ",", y_max, ")")

                for i in range(1, len(colors)):  # middle
                    y_color_min = colors[i - 1]["range"]
                    y_color_max = min(colors[i]["range"], y_max)
                    if y_color_max > y_color_min:
                        axis.fill_between(
                            [x_min, x_max], y_color_min, y_color_max,
                            facecolor=colors[i]["color"], alpha=alpha
                        )
                        print("Colored", colors[i]["color"], "(alpha =", alpha,
                              "from (", x_min, ",", y_color_min, ") to (",
                              x_max, ",", y_color_max, ")")

                axis.fill_between(
                    [x_min, x_max], min(y_max, colors[-1]["range"]), y_max,
                    facecolor=colors[-1]["color"], alpha=alpha
                )  # end

    def plot(self, x_val, y_val, label):
        """
        :param x_val: [] of *
            X-axis data
        :param y_val: [] of float
            List of values
        :param label: str
            Label of data points
        :return: void
            Plot data
        """

        if self.current_line >= self.max_lines_per_plot:  # new subplot
            if self.current_subcol >= self.sub_cols - 1:
                self.current_subrow += 1
                self.current_subcol = 0
            else:
                self.current_subcol += 1
            self.current_line = 0

        current_plot = self.current_subrow * self.sub_cols + self.current_subcol
        subplot = self.fig.axes[current_plot]
        subplot.plot(x_val, y_val, "-o", label=label)
        self.current_line += 1
        print("plot #", current_plot, ": (", x_val, ",", y_val, "):", label)

    def remove_empty_plots(self):
        for col in range(self.current_subcol + 1, self.sub_cols):  # same row
            self.axis[self.current_subrow][col].set_visible(False)

    def put_labels(self, x_label, y_label):
        self.fig.axes[0].set_ylabel(y_label)

        for axis in self.fig.axes:
            axis.set_xlabel(x_label)
            axis.legend()  # build legend

    def show(self, title, x_label, y_label, titles=None):
        """
        :param y_label: str
            Label of Y-axis
        :param x_label: str
            Label of X-axis
        :param title: str
            Title of plot
        :param keep_on_screen: bool
            True iff you want to keep plot on screen. False when you want to save it
        :return: void
            Shows plot
        """

        for i, axis in enumerate(self.fig.axes):
            try:
                axis.set_title(titles[i])
            except:
                print("Cannot set title in axis", i)

        self.remove_empty_plots()
        self.put_labels(x_label, y_label)
        plt.suptitle(title, fontsize=18)
        plt.show()
