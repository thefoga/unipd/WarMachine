# !/usr/bin/python3
# coding: utf-8

from datetime import timedelta


def generate_dates(since, until, days):
    """
    :param since: datetime
        Generate dates since this date
    :param until: datetime
        Generate dates until this date
    :param days: float
        Number of days between 2 consecutive dates
    :return: generator of datetime
        Dates in between boundaries and separated by exact interval
    """

    date = since
    while date <= until:
        yield date
        date += timedelta(days=days)
    yield until
