# !/usr/bin/python3
# coding: utf-8

from datetime import date
from enum import Enum

from core.risk import RiskName, RiskLevel


class DocsName(Enum):
    """ Risk names """

    SF = "Studio di Fattibilità"
    NP = "Norme di Progetto"
    PP = "Piano di Progetto"
    PQ = "Piano di Qualifica"
    GL = "Glossario"
    AR = "Analisi dei Requisiti"
    MU = "Manuale Utente"
    MS = "Manuale Sviluppatore"


# schedule variance (verify docs)
SVV_DATA = {
    DocsName.NP: [
        (date(day=9, month=3, year=2018), 1),
        (date(day=30, month=5, year=2018), 0),
        (date(day=25, month=6, year=2018), 1),
        (date(day=2, month=7, year=2018), 2),
        (date(day=10, month=7, year=2018), -1)
    ],
    DocsName.GL: [
        (date(day=12, month=4, year=2018), -2),
        (date(day=30, month=5, year=2018), -1),
        (date(day=25, month=6, year=2018), -1),
        (date(day=2, month=7, year=2018), 1),
        (date(day=10, month=7, year=2018), 2)
    ],
    DocsName.PP: [
        (date(day=30, month=3, year=2018), -1),
        (date(day=30, month=5, year=2018), -1),
        (date(day=25, month=6, year=2018), 1),
        (date(day=2, month=7, year=2018), 1),
        (date(day=10, month=7, year=2018), 0)
    ],
    DocsName.PQ: [
        (date(day=24, month=3, year=2018), -3),
        (date(day=30, month=5, year=2018), -2),
        (date(day=25, month=6, year=2018), 0),
        (date(day=2, month=7, year=2018), -1),
        (date(day=10, month=7, year=2018), -2)
    ],
    DocsName.AR: [
        (date(day=12, month=4, year=2018), 0),
        (date(day=30, month=5, year=2018), 0),
        (date(day=25, month=6, year=2018), 1),
        (date(day=2, month=7, year=2018), 3),
        (date(day=10, month=7, year=2018), 1)
    ],
    DocsName.MS: [
        (date(day=25, month=6, year=2018), 2),
        (date(day=2, month=7, year=2018), -1),
        (date(day=10, month=7, year=2018), 1)
    ],
    DocsName.MU: [
        (date(day=25, month=6, year=2018), 1),
        (date(day=2, month=7, year=2018), -1),
        (date(day=10, month=7, year=2018), 0)
    ]
}

# schedule variance (docs write)
SVW_DATA = {
    DocsName.NP: [
        (date(day=8, month=3, year=2018), 1),
        (date(day=30, month=5, year=2018), 1),
        (date(day=25, month=6, year=2018), 3),
        (date(day=2, month=7, year=2018), 2),
        (date(day=10, month=7, year=2018), 2)
    ],
    DocsName.GL: [
        (date(day=31, month=3, year=2018), 0),
        (date(day=30, month=5, year=2018), -1),
        (date(day=25, month=6, year=2018), 2),
        (date(day=2, month=7, year=2018), 2),
        (date(day=10, month=7, year=2018), 1)
    ],
    DocsName.PP: [
        (date(day=26, month=3, year=2018), 3),
        (date(day=30, month=5, year=2018), 2),
        (date(day=25, month=6, year=2018), -1),
        (date(day=2, month=7, year=2018), 2),
        (date(day=10, month=7, year=2018), 0)
    ],
    DocsName.PQ: [
        (date(day=18, month=3, year=2018), -1),
        (date(day=30, month=5, year=2018), -1),
        (date(day=25, month=6, year=2018), 1),
        (date(day=2, month=7, year=2018), 0),
        (date(day=10, month=7, year=2018), -1)
    ],
    DocsName.AR: [
        (date(day=4, month=4, year=2018), -4),
        (date(day=30, month=5, year=2018), -2),
        (date(day=25, month=6, year=2018), 0),
        (date(day=2, month=7, year=2018), 2),
        (date(day=10, month=7, year=2018), -1)
    ],
    DocsName.MS: [
        (date(day=25, month=6, year=2018), 1),
        (date(day=2, month=7, year=2018), 2),
        (date(day=10, month=7, year=2018), 2)
    ],
    DocsName.MU: [
        (date(day=25, month=6, year=2018), -2),
        (date(day=2, month=7, year=2018), -1),
        (date(day=10, month=7, year=2018), 0)
    ]
}

# cost variance (verify docs)
CVW_DATA = {
    DocsName.NP: [
        (
            date(day=8, month=3, year=2018), {
                "effettivo": 160,
                "pianificato": 220
            }
        ),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 300,
                "pianificato": 300
            }
        ),
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 75,
                "pianificato": 70
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 35,
                "pianificato": 40
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 40,
                "pianificato": 45
            }
        )
    ],
    DocsName.GL: [
        (
            date(day=31, month=3, year=2018), {
                "effettivo": 90,
                "pianificato": 100
            }),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 300,
                "pianificato": 300
            }
        ),
        (
            date(day=2, month=6, year=2018), {
                "effettivo": 90,
                "pianificato": 85
            }
        ),
        (
            date(day=1, month=7, year=2018), {
                "effettivo": 30,
                "pianificato": 30
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 75,
                "pianificato": 70
            }
        )
    ],
    DocsName.PP: [
        (date(day=26, month=3, year=2018), {
            "effettivo": 220,
            "pianificato": 280
        }),
        (date(day=30, month=5, year=2018), {
            "effettivo": 200,
            "pianificato": 220
        }),
        (
            date(day=30, month=6, year=2018), {
                "effettivo": 30,
                "pianificato": 30
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 90,
                "pianificato": 85
            }
        ),
        (
            date(day=12, month=7, year=2018), {
                "effettivo": 80,
                "pianificato": 90
            }
        )
    ],
    DocsName.PQ: [
        (date(day=18, month=3, year=2018), {
            "effettivo": 160,
            "pianificato": 200
        }),
        (date(day=30, month=5, year=2018), {
            "effettivo": 300,
            "pianificato": 350
        }),
        (
            date(day=30, month=6, year=2018), {
                "effettivo": 50,
                "pianificato": 50
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 95,
                "pianificato": 90
            }
        ),
        (
            date(day=12, month=7, year=2018), {
                "effettivo": 70,
                "pianificato": 65
            }
        )
    ],
    DocsName.AR: [
        (date(day=4, month=4, year=2018), {
            "effettivo": 480,
            "pianificato": 550
        }),
        (date(day=30, month=5, year=2018), {
            "effettivo": 300,
            "pianificato": 350
        }),
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 85,
                "pianificato": 80
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 85,
                "pianificato": 90
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 80,
                "pianificato": 75
            }
        )
    ],
    DocsName.MU: [
        (
            date(day=30, month=6, year=2018), {
                "effettivo": 80,
                "pianificato": 90
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 45,
                "pianificato": 50
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 55,
                "pianificato": 50
            }
        )
    ],
    DocsName.MS: [
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 95,
                "pianificato": 100
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 100,
                "pianificato": 105
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 85,
                "pianificato": 80
            }
        )
    ]
}

# cost variance (docs write)
CVV_DATA = {
    DocsName.NP: [
        (
            date(day=9, month=3, year=2018), {
                "effettivo": 60,
                "pianificato": 90
            }
        ),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 85,
                "pianificato": 90
            }
        ),
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 95,
                "pianificato": 100
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 170,
                "pianificato": 180
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 160,
                "pianificato": 180
            }
        )
    ],
    DocsName.GL: [
        (
            date(day=12, month=4, year=2018), {
                "effettivo": 30,
                "pianificato": 60
            }
        ),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 15,
                "pianificato": 40
            }
        ),
        (
            date(day=29, month=6, year=2018), {
                "effettivo": 150,
                "pianificato": 145
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 94,
                "pianificato": 100
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 20,
                "pianificato": 20
            }
        )
    ],
    DocsName.PP: [
        (
            date(day=30, month=3, year=2018), {
                "effettivo": 60,
                "pianificato": 90
            }
        ),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 100,
                "pianificato": 120
            }
        ),
        (
            date(day=28, month=6, year=2018), {
                "effettivo": 120,
                "pianificato": 135
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 95,
                "pianificato": 90
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 60,
                "pianificato": 80
            }
        )
    ],
    DocsName.PQ: [
        (
            date(day=24, month=3, year=2018), {
                "effettivo": 60,
                "pianificato": 90
            }
        ),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 120,
                "pianificato": 150
            }
        ),
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 95,
                "pianificato": 100
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 190,
                "pianificato": 185
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 60,
                "pianificato": 75
            }
        )
    ],
    DocsName.AR: [
        (
            date(day=12, month=4, year=2018), {
                "effettivo": 105,
                "pianificato": 165
            }
        ),
        (
            date(day=30, month=5, year=2018), {
                "effettivo": 90,
                "pianificato": 105
            }
        ),
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 90,
                "pianificato": 110
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 150,
                "pianificato": 160
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 200,
                "pianificato": 200
            }
        )
    ],
    DocsName.MU: [
        (
            date(day=30, month=6, year=2018), {
                "effettivo": 190,
                "pianificato": 180
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 105,
                "pianificato": 100
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 30,
                "pianificato": 30
            }
        )
    ],
    DocsName.MS: [
        (
            date(day=25, month=6, year=2018), {
                "effettivo": 80,
                "pianificato": 85
            }
        ),
        (
            date(day=2, month=7, year=2018), {
                "effettivo": 150,
                "pianificato": 155
            }
        ),
        (
            date(day=10, month=7, year=2018), {
                "effettivo": 50,
                "pianificato": 45
            }
        )
    ]
}

# indice gulpease
GULP_DATA = {
    DocsName.NP: [
        (date(day=9, month=3, year=2018), 43),
        (date(day=30, month=5, year=2018), 44),
        (date(day=30, month=6, year=2018), 40),
        (date(day=2, month=7, year=2018), 44),
        (date(day=10, month=7, year=2018), 46)
    ],
    DocsName.GL: [
        (date(day=12, month=4, year=2018), 52),
        (date(day=30, month=5, year=2018), 52),
        (date(day=2, month=7, year=2018), 51),
        (date(day=10, month=7, year=2018), 53)
    ],
    DocsName.PP: [
        (date(day=30, month=3, year=2018), 46),
        (date(day=30, month=5, year=2018), 53),
        (date(day=2, month=7, year=2018), 55),
        (date(day=10, month=7, year=2018), 61)
    ],
    DocsName.PQ: [
        (date(day=24, month=3, year=2018), 42),
        (date(day=30, month=5, year=2018), 49),
        (date(day=1, month=7, year=2018), 55),
        (date(day=10, month=7, year=2018), 70)
    ],
    DocsName.AR: [
        (date(day=12, month=4, year=2018), 42),
        (date(day=30, month=5, year=2018), 79),
        (date(day=2, month=7, year=2018), 80),
        (date(day=10, month=7, year=2018), 92)
    ],
    DocsName.MS: [
        (date(day=30, month=6, year=2018), 66),
        (date(day=2, month=7, year=2018), 45),
        (date(day=10, month=7, year=2018), 56)
    ],
    DocsName.MU: [
        (date(day=29, month=6, year=2018), 69),
        (date(day=2, month=7, year=2018), 40),
        (date(day=10, month=7, year=2018), 46)
    ]
}

# errori concettuali
ERR_DATA = {
    DocsName.NP: [
        (date(day=10, month=4, year=2018), 1.5),
        (date(day=30, month=5, year=2018), 1.1),
        (date(day=25, month=6, year=2018), 1.7),
        (date(day=2, month=7, year=2018), 1.5),
        (date(day=10, month=7, year=2018), 1.6)
    ],
    DocsName.GL: [
        (date(day=11, month=4, year=2018), 1.3),
        (date(day=30, month=5, year=2018), 1.9),
        (date(day=25, month=6, year=2018), 1.6),
        (date(day=2, month=7, year=2018), 1.5),
        (date(day=10, month=7, year=2018), 1.4)
    ],
    DocsName.PP: [
        (date(day=12, month=4, year=2018), 2.9),
        (date(day=30, month=5, year=2018), 0.9),
        (date(day=25, month=6, year=2018), 1.7),
        (date(day=2, month=7, year=2018), 1.3),
        (date(day=10, month=7, year=2018), 1.1)
    ],
    DocsName.PQ: [
        (date(day=13, month=4, year=2018), 1.1),
        (date(day=30, month=5, year=2018), 2.5),
        (date(day=25, month=6, year=2018), 2.0),
        (date(day=2, month=7, year=2018), 2.2),
        (date(day=10, month=7, year=2018), 2.1)
    ],
    DocsName.AR: [
        (date(day=13, month=4, year=2018), 0.8),
        (date(day=30, month=5, year=2018), 0.5),
        (date(day=25, month=6, year=2018), 1.9),
        (date(day=2, month=7, year=2018), 2.5),
        (date(day=10, month=7, year=2018), 1.6)
    ],
    DocsName.MS: [
        (date(day=25, month=6, year=2018), 1.7),
        (date(day=2, month=7, year=2018), 1.5),
        (date(day=10, month=7, year=2018), 1.2)
    ],
    DocsName.MU: [
        (date(day=25, month=6, year=2018), 1.4),
        (date(day=2, month=7, year=2018), 1.8),
        (date(day=10, month=7, year=2018), 1.1)
    ]
}

# rischi
RISK_DATA = {
    RiskName.IUS: [
        (date(day=15, month=3, year=2018), RiskLevel.MINORE),
        (date(day=17, month=3, year=2018), RiskLevel.MINORE),
        (date(day=20, month=6, year=2018), RiskLevel.INSIGNIFICANTE)
    ],
    RiskName.ILT: [
        (date(day=18, month=3, year=2018), RiskLevel.MAGGIORE),
        (date(day=22, month=3, year=2018), RiskLevel.MINORE)
    ],
    RiskName.PC: [
        (date(day=26, month=3, year=2018), RiskLevel.INSIGNIFICANTE),
        (date(day=30, month=3, year=2018), RiskLevel.MINORE),
        (date(day=3, month=4, year=2018), RiskLevel.MINORE),
        (date(day=16, month=4, year=2018), RiskLevel.INSIGNIFICANTE),
        (date(day=18, month=4, year=2018), RiskLevel.MINORE),
        (date(day=29, month=4, year=2018), RiskLevel.MINORE),
        (date(day=30, month=4, year=2018), RiskLevel.MINORE),
        (date(day=9, month=5, year=2018), RiskLevel.MINORE),
        (date(day=10, month=5, year=2018), RiskLevel.MINORE),
        (date(day=16, month=5, year=2018), RiskLevel.INSIGNIFICANTE),
        (date(day=18, month=5, year=2018), RiskLevel.MINORE),
        (date(day=21, month=5, year=2018), RiskLevel.INSIGNIFICANTE),
        (date(day=23, month=5, year=2018), RiskLevel.MINORE),
        (date(day=27, month=6, year=2018), RiskLevel.MINORE),
        (date(day=30, month=6, year=2018), RiskLevel.INSIGNIFICANTE),
        (date(day=3, month=7, year=2018), RiskLevel.MINORE)
    ],
    RiskName.ITT: [
        (date(day=27, month=4, year=2018), RiskLevel.MINORE),
        (date(day=30, month=4, year=2018), RiskLevel.MINORE),
        (date(day=17, month=5, year=2018), RiskLevel.MINORE)
    ],
    RiskName.CR: [
        (date(day=14, month=3, year=2018), RiskLevel.MINORE),
        (date(day=30, month=3, year=2018), RiskLevel.MINORE)
    ],
    RiskName.IIM: [
        (date(day=26, month=4, year=2018), RiskLevel.MINORE),
        (date(day=27, month=4, year=2018), RiskLevel.MINORE),
        (date(day=3, month=5, year=2018), RiskLevel.MAGGIORE),
        (date(day=4, month=5, year=2018), RiskLevel.MAGGIORE),
        (date(day=5, month=5, year=2018), RiskLevel.MAGGIORE)
    ],
    RiskName.SCA: [
        (date(day=2, month=5, year=2018), RiskLevel.MAGGIORE)
    ]
}

# process efficiency
PE_DATA = {
    RiskName.IUS: [
        (date(day=30, month=5, year=2018), 92),
        (date(day=25, month=6, year=2018), 88),
        (date(day=2, month=7, year=2018), 95),
        (date(day=10, month=7, year=2018), 89)
    ],
    RiskName.ILT: [
        (date(day=30, month=5, year=2018), 85),
        (date(day=25, month=6, year=2018), 81),
        (date(day=2, month=7, year=2018), 80),
        (date(day=10, month=7, year=2018), 88)
    ],
    RiskName.PC: [
        (date(day=30, month=5, year=2018), 79),
        (date(day=25, month=6, year=2018), 83),
        (date(day=2, month=7, year=2018), 89),
        (date(day=10, month=7, year=2018), 75)
    ],
    RiskName.ITT: [
        (date(day=30, month=5, year=2018), 91),
        (date(day=25, month=6, year=2018), 90),
        (date(day=2, month=7, year=2018), 85),
        (date(day=10, month=7, year=2018), 91)
    ],
    RiskName.CR: [
        (date(day=30, month=5, year=2018), 81),
        (date(day=25, month=6, year=2018), 86),
        (date(day=2, month=7, year=2018), 89),
        (date(day=10, month=7, year=2018), 82)
    ],
    RiskName.IIM: [
        (date(day=30, month=5, year=2018), 77),
        (date(day=25, month=6, year=2018), 79),
        (date(day=2, month=7, year=2018), 85),
        (date(day=10, month=7, year=2018), 79)
    ],
    RiskName.SCA: [
        (date(day=30, month=5, year=2018), 83),
        (date(day=25, month=6, year=2018), 86),
        (date(day=2, month=7, year=2018), 90),
        (date(day=10, month=7, year=2018), 85)
    ]
}
