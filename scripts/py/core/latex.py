# !/usr/bin/python3
# coding: utf-8

import os
from collections import OrderedDict
from core.files import get_lines_of_file

LATEX_INPUT_REGEX = "\\include{"


def is_tex_comment(line):
    """
    :param line: str
        Line of .tex file
    :return: bool
        True iff line is a comment
    """

    return line.strip().startswith("%")


def get_tex_includes(path, root_folder):
    """
    :param path: str
        Path to tex file
    :param root_folder: str
        Root folder of doc
    :return: [] of str
        Files included in main doc
    """

    lines = get_lines_of_file(path)
    sections = [
        line.replace(LATEX_INPUT_REGEX, "").replace("}", "").strip() + ".tex"
        for line in lines
        if LATEX_INPUT_REGEX in line and not is_tex_comment(line)
    ]
    sections = [
        os.path.join(root_folder, section) for section in sections
    ]
    for section in sections:
        sections += get_tex_includes(section, root_folder)  # recursive call
    return list(OrderedDict.fromkeys(sections))


def write_glossary(glossary, path):
    """
    :param glossary: Counter
        Counter with words and number of occurrence
    :param path: str
        Path to output file
    :return: void
        Converts glossary to latex and writes result
    """

    out = ""
    last_letter = None

    for key in glossary:
        definition = key if key[0].isupper() else key.title()
        title_letter = key[0].upper() if key[
                                             0].upper() != last_letter else last_letter

        if title_letter != last_letter:
            if last_letter is not None:
                out += "\n\t\\newpage\n\n"
            out += "\\section{" + title_letter + "}\n"
            last_letter = title_letter

        out += "\n\t\\subsection{\\textit{" + definition + "}}\n\t\t\\todo\n"

    with open(path, "w") as writer:
        writer.write(out)
