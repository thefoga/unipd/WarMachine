# !/usr/bin/python3
# coding: utf-8

from enum import Enum


class RiskLevel(Enum):
    """ Risk levels """

    INSIGNIFICANTE = 0
    MINORE = 1
    MAGGIORE = 2
    MATERIALE = 3
    CATASTROFICO = 4


class RiskName(Enum):
    """ Risk names """

    ITT = "ITT"
    GHS = "GHS"
    PD = "PD"
    PC = "PC"
    IIM = "IIM"
    ILT = "ILT"
    ROR = "ROR"
    IUS = "IUS"
    CR = "CR"
    SCA = "SCA"
