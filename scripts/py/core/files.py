# !/usr/bin/python3
# coding: utf-8

import os

from core.cmd import detex


def find_files(root_folder, extension=None):
    """
    :param root_folder: str
        Folder to scan
    :param extension: str or None
        Finds files ending with just this extension. None means any extension
    :return: [] of str
        List of files found in folder
    """

    lst = []
    for fil in os.listdir(root_folder):
        if os.path.isdir(os.path.join(root_folder, fil)):
            lst += find_files(
                os.path.join(root_folder, fil), extension
            )  # get list of files in directory
        else:  # this is a file
            if extension is not None:
                if fil.endswith(extension):
                    lst.append(os.path.join(root_folder, fil))
    return lst


def get_file(path):
    """
    :param path: str
        Path of file to read
    :return: str
        Content of file
    """

    with open(path, "r") as reader:
        return reader.read()


def get_lines_of_file(path, encoding="utf-8"):
    """
    :param path: str
        Path of file to read
    :param encoding: str
        Which encoding to use when parsing file
    :return: [] of str
        Lines in file
    """

    raw = get_file(path)
    lines = raw.split("\n")
    return [
        bytes(line, encoding).decode(encoding, "ignore")
        for line in lines
    ]


def get_content_of_file(path, encoding="utf-8"):
    """
    :param path: str
        Path of file to read
    :param encoding: str
        Which encoding to use when parsing file
    :return: str
        Content of file
    """

    lines = get_lines_of_file(path, encoding=encoding)
    return "\n".join(lines)


def get_content_of_text_file(path):
    """
    :param path: str
        Path of file to read
    :return: str
        Content of file
    """

    with open(path, "r") as reader:
        return reader.read()


def get_content_of_latex_file(path):
    """
    :param path: str
        Path of .tex file to read
    :return: str
        Content of file
    """

    return detex(path)
