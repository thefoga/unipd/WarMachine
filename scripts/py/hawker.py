# !/usr/bin/python3
# coding: utf-8

import argparse
import os

from matplotlib import dates as mdates

from core.charts import SubPlotter
from core.docs import CVW_DATA, SVW_DATA, GULP_DATA, SVV_DATA, \
    ERR_DATA, RISK_DATA, CVV_DATA, PE_DATA
from core.risk import RiskLevel
from utils import filter_data

PLOT_XTICKS = [
    "13 " + month for month in
    ["Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre"]
]


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-o <output folder> "
                                           "-h for full usage")
    parser.add_argument("-o", dest="out",
                        help="output folder", required=True)
    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    assert os.path.exists(args.out)

    return args.out


def plot_cvw():
    y_ticks = [0] + list(range(75, 130, 5))
    all_dates = []
    for doc, data in CVW_DATA.items():
        all_dates += [
            point[0] for point in data
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),
            "ticks": filter_data(all_dates)
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for doc, data in CVW_DATA.items():
        y = [
            val[1]["effettivo"] / val[1]["pianificato"] * 100.0
            for val in data
        ]
        x = [
            val[0] for val in data
        ]

        plotter.plot(x, y, doc.value)

    plotter.plot_background(
        [
            {
                "range": 90,
                "color": "green"
            },
            {
                "range": 125,
                "color": "yellow"
            },
            {
                "range": 150,
                "color": "red"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Cost variance (produzione documenti)",
        x_label="",
        y_label="% cost variance",
        # titles for each plot
    )


def plot_cvv():
    y_ticks = [0] + list(range(75, 130, 5))
    all_dates = []
    for doc, data in CVV_DATA.items():
        all_dates += [
            point[0] for point in data
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),
            "ticks": filter_data(all_dates)
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for doc, data in CVV_DATA.items():
        y = [
            val[1]["effettivo"] / val[1]["pianificato"] * 100.0
            for val in data
        ]
        x = [
            val[0] for val in data
        ]

        plotter.plot(x, y, doc.value)

    plotter.plot_background(
        [
            {
                "range": 90,
                "color": "green"
            },
            {
                "range": 125,
                "color": "yellow"
            },
            {
                "range": 150,
                "color": "red"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Cost variance (verifica documenti)",
        x_label="",
        y_label="% cost variance",
        # titles for each plot
    )


def plot_gulp():
    y_ticks = list(range(35, 100, 5))
    all_dates = []
    for risk, dates in GULP_DATA.items():
        all_dates += [
            date[0] for date in dates
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),  # dates,
            "ticks": filter_data(all_dates)  # dates
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for doc, data in GULP_DATA.items():
        plotter.plot(
            [
                point[0] for point in data
            ],
            [
                point[1] for point in data
            ],
            doc.value
        )

    plotter.plot_background(
        [
            {
                "range": 40,
                "color": "red"
            },
            {
                "range": 60,
                "color": "yellow"
            },
            {
                "range": 100,
                "color": "green"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Indice Gulpease",
        x_label="Date di fine verifica documenti",
        y_label="Gulpease"
        # titles for each plot
    )


def plot_svw():
    y_ticks = list(range(-5, 7))
    all_dates = []
    for risk, dates in SVW_DATA.items():
        all_dates += [
            date[0] for date in dates
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),  # dates,
            "ticks": filter_data(all_dates)  # dates
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for doc, data in SVW_DATA.items():
        plotter.plot(
            [
                point[0] for point in data
            ],
            [
                point[1] for point in data
            ],
            doc.value
        )

    plotter.plot_background(
        [
            {
                "range": 0,
                "color": "green"
            },
            {
                "range": 4,
                "color": "yellow"
            },
            {
                "range": 8,
                "color": "red"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Schedule variance (redazione documenti)",
        x_label="Date di fine redazione documenti",
        y_label="Schedule variance"
        # titles for each plot
    )


def plot_svv():
    y_ticks = list(range(-5, 7))
    all_dates = []
    for risk, dates in SVV_DATA.items():
        all_dates += [
            date[0] for date in dates
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),  # dates,
            "ticks": filter_data(all_dates)  # dates
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for doc, data in SVV_DATA.items():
        plotter.plot(
            [
                point[0] for point in data
            ],
            [
                point[1] for point in data
            ],
            doc.value
        )

    plotter.plot_background(
        [
            {
                "range": 0,
                "color": "green"
            },
            {
                "range": 4,
                "color": "yellow"
            },
            {
                "range": 8,
                "color": "red"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Schedule variance (verifica documenti)",
        x_label="Date di fine verifica documenti",
        y_label="Schedule variance",

        # titles for each plot
    )


def plot_err():
    y_ticks = list(range(0, 5))
    all_dates = []
    for risk, dates in ERR_DATA.items():
        all_dates += [
            date[0] for date in dates
        ]
    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),
            "ticks": filter_data(all_dates)
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for doc, data in ERR_DATA.items():
        plotter.plot(
            [
                point[0] for point in data
            ],
            [
                point[1] for point in data
            ],
            doc.value
        )

    plotter.plot_background(
        [
            {
                "range": 2,
                "color": "green"
            },
            {
                "range": 4,
                "color": "yellow"
            },
            {
                "range": 10,
                "color": "red"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Errori concettuali",
        x_label="Documenti",
        y_label="%",

        # titles for each plot
    )


def plot_risk():
    y_ticks = list(range(0, 5))
    all_dates = []
    for risk, dates in RISK_DATA.items():
        all_dates += [
            date[0] for date in dates
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),  # dates,
            "ticks": filter_data(all_dates)  # dates
        },
        {
            "range": y_ticks,
            "ticks": [risk.name for risk in RiskLevel]
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for risk, points in RISK_DATA.items():
        plotter.plot(
            [
                point[0] for point in points
            ],  # x
            [
                point[1].value for point in points
            ],  # y
            risk.name  # label
        )

    plotter.plot_background(
        [
            {
                "range": 1,
                "color": "green"
            },
            {
                "range": 3,
                "color": "yellow"
            },
            {
                "range": 5,
                "color": "red"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Attualizzazione rischi",
        x_label="Tempo",
        y_label="Grado d'impatto"
    )


def plot_pe():
    y_ticks = list(range(75, 100, 5))
    all_dates = []
    for risk, dates in PE_DATA.items():
        all_dates += [
            date[0] for date in dates
        ]

    plotter = SubPlotter(sub_rows=1, sub_cols=2, max_lines_per_plot=4)
    plotter.setup(
        {
            "range": filter_data(all_dates),  # dates,
            "ticks": filter_data(all_dates)  # dates
        },
        {
            "range": y_ticks,
            "ticks": y_ticks
        }
    )

    for axis in plotter.fig.axes:
        axis.xaxis.set_minor_formatter(mdates.DateFormatter("%d %B"))
        axis.xaxis.set_major_formatter(mdates.DateFormatter("%d %B"))

    for risk, points in PE_DATA.items():
        plotter.plot(
            [
                point[0] for point in points
            ],  # x
            [
                point[1] for point in points
            ],  # y
            risk.name  # label
        )

    plotter.plot_background(
        [
            {
                "range": 75,
                "color": "red"
            },
            {
                "range": 85,
                "color": "yellow"
            },
            {
                "range": 100,
                "color": "green"
            }
        ],
        alpha=0.2
    )

    plotter.show(
        title="Process efficiency",
        x_label="Tempo",
        y_label="PE"
    )


if __name__ == "__main__":
    # plot_cvw()
    # plot_cvv()
    # plot_gulp()
    # plot_svv()
    plot_svw()
    # plot_err()
    # plot_risk()
    # plot_pe()
