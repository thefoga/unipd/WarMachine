# !/usr/bin/python3
# coding: utf-8

"""
Readability indices: higher scores imply "easier" reading
"""

from readability.utils import preprocess, letter_count, sentence_count, \
    word_count, \
    avg_words_per_sentence, avg_syllables_per_word, syllable_count


def flesch_kincaid_ease(text):
    """ http://en.wikipedia.org/wiki/Flesch%E2%80
    %93Kincaid_readability_tests#Flesch_Reading_Ease """

    text = preprocess(text)
    return 206.835 - (1.015 * avg_words_per_sentence(text)) - (
        84.6 * avg_syllables_per_word(text))


def douma(text):
    """ Variant of Flesch-Kincaid for Dutch:
    http://www.cnts.ua.ac.be/papers/2002/Geudens02.pdf """

    text = preprocess(text)
    return 206.84 - (0.33 * avg_words_per_sentence(text)) - (
        0.77 * avg_syllables_per_word(text))


def kandel_moles(text):
    """ Variant of Flesch-Kincaid for French (citation not easily traceable)
    """

    text = preprocess(text)
    return 209 - (1.15 * avg_words_per_sentence(text)) - (
        0.68 * avg_syllables_per_word(text))


def gulpease(text):
    """ https://it.wikipedia.org/wiki/Indice_Gulpease """

    text = preprocess(str(text))
    return 89.0 + \
           (300.0 * sentence_count(text) - 10.0 * letter_count(text)) \
           / (word_count(text))


def fernandez_huerta(text):
    """ Developed for Spanish texts (citation not easily traceable) """

    text = preprocess(text)
    factor = 100.0 / word_count(text)
    return 206.84 - (0.6 * factor * syllable_count(text)) - (
        1.02 * factor * sentence_count(text))
