# !/usr/bin/python3
# coding: utf-8

INPUT_FILES = [
    "/ra/esterni/analisi-dei-requisiti/Analisi_Dei_Requisiti_v4.0.0.tex",
    "/ra/esterni/piano-di-progetto/Piano_Di_Progetto_v4.0.0.tex",
    "/ra/esterni/piano-di-qualifica/Piano_Di_Qualifica_v4.0.0.tex",
    "/ra/interni/norme-di-progetto/Norme_Di_Progetto_v4.0.0.tex",
    "/ra/esterni/glossario/Glossario_v3.0.0.tex",
    "/ra/esterni/manuali/manuale-sviluppatore/Manuale_Sviluppatore_v2.0.0.tex",
    "/ra/esterni/manuali/manuale-utente/Manuale_Utente_v2.0.0.tex"
]
