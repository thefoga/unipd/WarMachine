# !/usr/bin/python3
# coding: utf-8

"""
Various utility functions
"""

import re


def preprocess(text):
    text = bytes(text, "ascii").decode("ascii", "ignore")  # convert to ASCII
    # if the input is HTML, force-add full stops after these tags
    fullStopTags = ['li', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'dd']
    for tag in fullStopTags:
        text = re.sub(r'</' + tag + '>', '.', text)
    text = re.sub(r'<[^>]+>', '', text)  # strip out HTML
    text = re.sub(r'[,:;()\-]', ' ',
                  text)  # replace commas, hyphens etc (count as spaces)
    text = re.sub(r'[\.!?]', '.', text)  # unify terminators
    text = re.sub(r'^\s+', '', text)  # strip leading whitespace
    text = re.sub(r'[ ]*(\n|\r\n|\r)[ ]*', ' ',
                  text)  # replace new lines with spaces
    text = re.sub(r'([\.])[\. ]+', '.',
                  text)  # check for duplicated terminators
    text = re.sub(r'[ ]*([\.])', '. ', text)  # pad sentence terminators
    text = re.sub(r'\s+', ' ', text)  # remove multiple spaces
    text = re.sub(r'\s+$', '', text);  # strip trailing whitespace
    return text


def letter_count(text):
    """ Gives letter count (ignores non-letters). """

    text = preprocess(text)
    newText = re.sub(r'[^A-Za-z]+', '', text)
    return len(newText)


def sentence_count(text):
    text = preprocess(text)
    # note: might be tripped up by honorifics and abbreviations
    return max(1, len(re.sub(r'[^\.!?]', '', text)))


def word_count(text):
    text = preprocess(text)
    return 1 + len(re.sub(r'[^ ]', '', text))  # space count + 1 is word count


def avg_words_per_sentence(text):
    text = preprocess(text)
    return 1.0 * word_count(text) / sentence_count(text)


def total_syllables(text):
    text = preprocess(text)
    words = text.split()
    return sum([syllable_count(w) for w in words])


def avg_syllables_per_word(text):
    text = preprocess(text)
    num_words = word_count(text)
    words = text.split()
    num_syllables = sum([syllable_count(w) for w in words])
    return 1.0 * num_syllables / num_words


def six_letter_word_count(text, use_proper_nouns=True):
    text = preprocess(text)
    num_long_words = 0
    words = text.split()
    for word in words:
        if len(word) >= 6:
            if use_proper_nouns or word[:1].islower():
                num_long_words += 1
    return num_long_words


def three_syllable_word_count(text, use_proper_nouns=True):
    text = preprocess(text)
    num_long_words = 0
    words = text.split()
    for word in words:
        if syllable_count(word) >= 3:
            if use_proper_nouns or word[:1].islower():
                num_long_words += 1
    return num_long_words


def percent_three_syllable_words(text, use_proper_nouns=True):
    text = preprocess(text)
    return 100.0 * three_syllable_word_count(text,
                                             use_proper_nouns) / word_count(
        text)


def syllable_count(word):
    """ Pretty good heuristic: treat consecutive vowel strings as syllables.
    """

    word = word.lower()
    # remove non-alphanumeric characters
    word = re.sub(r'[^a-z]', '', word)
    word_bits = re.split(r'[^aeiouy]+', word)
    num_bits = 0
    for wb in word_bits:
        if wb != '':
            num_bits += 1
    return max(1, num_bits)
