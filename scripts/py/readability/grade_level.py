# !/usr/bin/python3
# coding: utf-8

"""
Grade level estimators: higher scores imply more advanced-level material
"""

from math import sqrt

from algs.utils import preprocess, letter_count, sentence_count, word_count, \
    avg_words_per_sentence, avg_syllables_per_word, three_syllable_word_count, \
    percent_three_syllable_words


def flesch_kincaid_grade(text):
    """ http://en.wikipedia.org/wiki/Flesch%E2%80
    %93Kincaid_readability_tests#Flesch.E2.80.93Kincaid_Grade_Level """

    text = preprocess(text)
    return (0.39 * avg_words_per_sentence(text)) + (
        11.8 * avg_syllables_per_word(text)) - 15.59


def gunning_fog(text):
    """ http://en.wikipedia.org/wiki/Gunning_Fog_Index """

    text = preprocess(text)
    return 0.4 * (
        avg_words_per_sentence(text) + percent_three_syllable_words(text,
                                                                    False))


def coleman_liau(text):
    """ http://en.wikipedia.org/wiki/Coleman-Liau_Index """

    text = preprocess(text)
    return (5.89 * letter_count(text) / word_count(text)) - (
        0.3 * sentence_count(text) / word_count(text)) - 15.8


def smog(text):
    """ http://en.wikipedia.org/wiki/SMOG_Index """

    text = preprocess(text)
    return 1.043 * sqrt((three_syllable_word_count(text) * (
        30.0 / sentence_count(text))) + 3.1291)


def ari(text):
    """ http://en.wikipedia.org/wiki/Automated_Readability_Index """

    text = preprocess(text)
    return (4.71 * letter_count(text) / word_count(text)) + (
        0.5 * word_count(text) / sentence_count(text)) - 21.43
