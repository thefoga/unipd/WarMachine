# !/usr/bin/python3
# coding: utf-8

"""
Other indices (not grade levels): higher scores imply "more difficult" reading
"""

from algs.utils import preprocess, word_count, six_letter_word_count, \
    sentence_count


def lix(text):
    """ http://en.wikipedia.org/wiki/LIX """

    text = preprocess(text)
    num_words = word_count(text)
    return (100.0 * six_letter_word_count(text) / num_words) + (
        1.0 * num_words / sentence_count(text))


def rix(text):
    """ More generalized variant of LIX """

    text = preprocess(text)
    return 1.0 * six_letter_word_count(text) / sentence_count(text)
