# !/usr/bin/python3
# coding: utf-8

"""
readability.py

Burr Settles (2013)
burrsettles.com

Simple python module that implements several different readability metrics 
for Western European languages.

For a pretty thorough overview of these and other metrics, see:
http://www.ideosity.com/ourblog/post/ideosphere-blog/2010/01/14/readability-tests-and-formulas

Also: http://en.wikipedia.org/wiki/Category:Readability_tests
"""

from algs.grade_level import flesch_kincaid_grade, gunning_fog, coleman_liau, \
    smog, ari
from algs.misc import lix, rix
from algs.readability import flesch_kincaid_ease, douma, kandel_moles, \
    gulpease, fernandez_huerta


def readability_metrics(text):
    """ Returns a dictionary containing all readability scores for a given
    text """

    return {
        'flesch_kincaid_ease': flesch_kincaid_ease(text),
        'readability': gulpease(text),
        'douma': douma(text),
        'kandel_moles': kandel_moles(text),
        'fernandez_huerta': fernandez_huerta(text),
        'flesch_kincaid_grade': flesch_kincaid_grade(text),
        'gunning_fog': gunning_fog(text),
        'coleman_liau': coleman_liau(text),
        'smog': smog(text),
        'ari': ari(text),
        'lix': lix(text),
        'rix': rix(text)
    }
