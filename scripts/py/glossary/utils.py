# !/usr/bin/python3
# coding: utf-8

import os
from collections import Counter

import regex

from core.files import get_file, find_files, get_lines_of_file
from core.latex import get_tex_includes

LATEX_COMMAND_REGEX = r"(\gl{(?:[^{}]|(?0))*\})"
GLOSSARY_INDEX = "\\subsection{\\textit{"
GLOSSARY_REGEX = "\\gl"


def lst_to_glossary(lst):
    """
    :param lst: [] of str
        List of raw glossary terms
    :return: [] of str
        Glossary equivalent
    """

    return list([
        word.lower() for word in lst
    ])


def find_glossary_words_file(path):
    """
    :param path: str
        Path of file to read
    :return: {} str -> int
        Dictionary of glossary words found in file
    """

    raw = get_file(path)
    lines = raw.split("\n")
    all_words = []
    for i, line in enumerate(lines):
        line = bytes(line, "utf-8").decode("utf-8", "ignore")
        lines[i] = line
        all_words += line.split()

    raw = "\n".join(lines)
    tokens = regex.findall(LATEX_COMMAND_REGEX, raw)
    tokens = [
        token.replace("gl{", "").replace("}", "") for token in tokens
    ]
    return tokens, all_words


def find_glossary_words_folder(folder):
    """
    :param folder: str
        Folder to scan
    :return: Counter
        Counter with words and number of occurrence
    """

    files = find_files(folder, extension=".tex")
    glossary = []
    for fil in files:
        glossary_words, all_words = find_glossary_words_file(fil)
        glossary += glossary_words
    return Counter(lst_to_glossary(glossary))  # count occurrences


def parse_glossary_file(path):
    """
    :param path: str
        Path of file to read
    :return: {}
        List of terms in glossary
    """

    raw = [
        line.replace(GLOSSARY_INDEX, "").replace("}}", "").strip()
        for line in get_lines_of_file(path) if GLOSSARY_INDEX in line
    ]
    return Counter(lst_to_glossary(raw))


def get_all_written_glossary_words(files):
    """
    :param files: [] of str
        Files to scan
    :return: Counter
        Counter with words and number of occurrence
    """

    glossary = []
    for fil in files:
        glossary_words, all_words = find_glossary_words_file(fil)
        glossary += glossary_words
        included_files = get_tex_includes(fil, os.path.dirname(fil))

        for incl_fil in included_files:  # scan inclusions
            glossary_words, all_words = find_glossary_words_file(incl_fil)
            glossary += glossary_words

    return Counter(lst_to_glossary(glossary))  # count occurrences


def get_message_occur(occurrence):
    """
    :param occurrence: int
        Number of times word appears in text
    :return: str
        Advise message
    """

    if occurrence < 0:
        return "missing in glossary"

    if occurrence == 0:
        return "to be removed from glossary"

    if occurrence == 1:
        return "OK"

    return "too many!!!"
