# !/usr/bin/python3
# coding: utf-8

INPUT_FILES = [
    "/rq/esterni/glossario/Glossario_v3.0.0.tex",
    "/rq/esterni/analisi-dei-requisiti/Analisi_Dei_Requisiti_v3.0.0.tex",
    "/rq/esterni/piano-di-progetto/Piano_Di_Progetto_v3.0.0.tex",
    "/rq/esterni/piano-di-qualifica/Piano_Di_Qualifica_v3.0.0.tex",
    "/rq/esterni/manuali/manuale-sviluppatore/Manuale_Sviluppatore_v1.0.0.tex",
    "/rq/esterni/manuali/manuale-utente/Manuale_Utente_v1.0.0.tex",
    "/rq/esterni/lettera-di-presentazione/Lettera_Di_Presentazione.tex",
    "/rq/interni/norme-di-progetto/Norme_Di_Progetto_v3.0.0.tex"
]
