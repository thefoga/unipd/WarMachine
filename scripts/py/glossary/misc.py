# !/usr/bin/python3
# coding: utf-8


import argparse
import os

from core.pretty_table import pretty_format_table
from glossary.utils import get_message_occur

CASE_INSENSITIVE_REGEX = "(?i)"
NOT_ALPHANUMERIC_REGEX = "[^a-zA-Z0-9_/]"
END_NOT_ALPHANUMERIC = "[^a-zA-Z0-9_/]"


def any_case_regex(word):
    """
    :param word: str
        Word to find
    :return: str
        Regex
    """

    return NOT_ALPHANUMERIC_REGEX + CASE_INSENSITIVE_REGEX + word + END_NOT_ALPHANUMERIC


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(
        usage="-f <folder to scan> -g <glossary to use> -o <output file>"
              "-h for full usage")
    parser.add_argument("-f", dest="fol",
                        help="folder to scan", type=str, required=True)
    parser.add_argument("-g", dest="glossary",
                        help="glossary to scan", type=str, required=False)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()
    folder = args.fol
    assert os.path.exists(folder)

    glossary = args.glossary or None
    if glossary:
        assert os.path.exists(glossary)

    return folder, glossary


def preserve_case_replacement(replacement, new_start, new_end, start, match):
    """
    :param replacement: str
        Replacement
    :param new_start: str
        Start replacement with this. Normally it's ""
    :param new_end: str
        End replacement with this. Normally it's ""
    :param start: str
        Where to count case (start of word). Normally it's 0.
    :param match: str
        Match found
    :return: str
        Replacement
    """

    match = match.group()  # raw match

    if match.islower():
        out = replacement.lower()
    else:
        if match.isupper():
            out = replacement.upper()
        else:
            out = replacement.title()

    out = new_start + out + new_end
    if new_end != "":
        out = match[0] + out + match[-1]
    return out


def print_words(in_glossary, in_text, title, first_occurrences={}):
    """
    :param in_glossary: Counter
        Words in glossary file
    :param in_text: Counter
        Words marked glossary in text
    :param title: str
        Title to print
    :param first_occurrences: {}
        Dictionary <str -> str> that for any glossary word. has the name of
        the file with the first occurrence
    :return: void
        Debugs list of words
    """

    labels = [
        "glossary word", "# occurrences", "first occurrence", "note"
    ]
    data = []

    for word, occurrence in sorted(in_text.items(), key=lambda x: x[0]):
        first_occurrence_file = first_occurrences[word] \
            if word in first_occurrences else "not found"

        message = get_message_occur(occurrence)
        if word not in in_glossary:
            message += ", " + get_message_occur(-1)

        data.append([word, occurrence, first_occurrence_file, message])

    if data:
        print("\n" + title)
        print(pretty_format_table(labels, data))
