import os
import re
from functools import partial

from core.cmd import join_paths
from core.files import get_content_of_file
from core.latex import get_tex_includes
from glossary.data import INPUT_FILES
from glossary.misc import parse_args, create_args, \
    preserve_case_replacement, CASE_INSENSITIVE_REGEX, NOT_ALPHANUMERIC_REGEX, \
    any_case_regex
from glossary.utils import parse_glossary_file, \
    get_all_written_glossary_words


def clean_text(text):
    return text\
                .replace("  ", " ")\
                .replace("\ \\", "\\")\
                .replace(". \gl{html}", ".html")\
                .replace(".\gl{html}", ".html")


def mark_doc(path, in_glossary):
    """
    :param path: str
        Path to tex file
    :param in_glossary: Counter
        Words in glossary file
    :return: void
        Marks first and first only entry of glossary words in file
    """

    included_files = get_tex_includes(path, os.path.dirname(path))

    # write glossary
    for word in in_glossary:  # for each word marked glossary
        for fil in included_files:  # remove all \gl notes and find first
            text = get_content_of_file(fil)  # read file
            text = re.sub(
                CASE_INSENSITIVE_REGEX + "\\\gl{" + word + "}",
                partial(
                    preserve_case_replacement, word, "", "", len("\\gl{") + 1
                ),
                text
            )  # remove all glossary occurrences
            text = clean_text(text)
            with open(fil, "w") as out:  # write results
                out.write(text)

        first_occurrence = None
        for fil in included_files:  # find if first occurrence
            text = get_content_of_file(fil)  # read file
            word_in_text = re.search(any_case_regex(word), text) is not None
            if first_occurrence is None and word_in_text:
                first_occurrence = fil

        # mark first occurrence
        if first_occurrence is not None:
            text = get_content_of_file(first_occurrence)
            text = re.sub(
                any_case_regex(word),
                partial(
                    preserve_case_replacement, word, " \\gl{", "}", 1
                ),
                text,
                1  # just first occurrence
            )
            text = clean_text(text)

            with open(first_occurrence, "w") as out:
                out.write(text)


def cli():
    """
    :return: void
        Parses files from input folder, then writes .tex output
    """

    folder, glossary_file = parse_args(create_args())
    files = [
        join_paths(folder, fil) for fil in INPUT_FILES
    ]
    in_glossary = parse_glossary_file(glossary_file)  # in glossary
    in_all = get_all_written_glossary_words(files)
    in_glossary += in_all

    for fil in files:
        print("Fix glossary words in", os.path.basename(fil))
        mark_doc(fil, in_glossary)


if __name__ == "__main__":
    cli()
