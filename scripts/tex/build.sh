# !/bin/bash
# coding: utf-8


# project settings
PROJECT_NAME="TexBuilder"
PROJECT_WEBPAGE="https://github.com/WarMachineSwe/WarMachine"
PROJECT_AUTHOR="sirfoga"
PROJECT_AUTHOR_EMAIL="sirfoga@protonmail.com"
PROJECT_DESCRIPTION="Build .tex to generate .pdfs."
PROJECT_ARGS="<.tex file to compile>"

# general settings
TEX_COMPILER=pdflatex

# help functions
function showHelp {
	echo $PROJECT_DESCRIPTION" Use as follows:"
    echo $PROJECT_ARGS
}

function errArgs {
    echo "Wrong or incorrect use of arguments."
    showHelp
}

function showDebugMessage() {
	echo
	echo "$1"
	echo
}

# check args
if [[ "$#" != 1 ]]; then
	errArgs
	exit 1
fi

# parse args
fileToCompile=$1
folderWhereToCompile=$(dirname $fileToCompile)

# compile
# todo debug only showDebugMessage "Compiling..."
cd $folderWhereToCompile
echo "---------------------------------------------------------"
$TEX_COMPILER $fileToCompile  #  &> /dev/null  # suppress output
outputFile="${fileToCompile/.tex/.pdf}"  # get output file

# remove
# todo debug only showDebugMessage "Cleaning..."
rm *.tex.backup &> /dev/null
rm *~ &> /dev/null
rm *.toc &> /dev/null
rm *.out &> /dev/null
rm *.log &> /dev/null
rm *.dvi &> /dev/null
rm *.aux &> /dev/null
rm *.lof &> /dev/null
rm *.ps &> /dev/null

exit 0