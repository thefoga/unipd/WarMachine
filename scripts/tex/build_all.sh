# !/bin/bash
# coding: utf-8


# files to compile
FILES=(
    "/ra/esterni/glossario/Glossario_v3.0.0.tex"
    "/ra/esterni/analisi-dei-requisiti/Analisi_Dei_Requisiti_v4.0.0.tex"
    "/ra/esterni/piano-di-progetto/Piano_Di_Progetto_v4.0.0.tex"
    "/ra/esterni/piano-di-qualifica/Piano_Di_Qualifica_v4.0.0.tex"
    "/ra/esterni/manuali/manuale-sviluppatore/Manuale_Sviluppatore_v2.0.0.tex"
    "/ra/esterni/manuali/manuale-utente/Manuale_Utente_v2.0.0.tex"
    # "/ra/esterni/lettera-di-presentazione/Lettera_Di_Presentazione.tex"
    "/ra/interni/norme-di-progetto/Norme_Di_Progetto_v4.0.0.tex"
)


ROOT_FOLDER=$(dirname $PWD)  # working dir
ROOT_FOLDER=$(dirname $ROOT_FOLDER)

# help functions
function compileFile() {
	echo "Compiling $1"
	bash build.sh $1
}

for f in ""${FILES[@]}""
do
	fullPath=$ROOT_FOLDER$f  # get full path
    compileFile $fullPath  # run
done

exit 0
