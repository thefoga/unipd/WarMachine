

## Expected Behavior
Create a link between two elements in the canvas.

## Actual Behavior
Does not create a link between two Controller elements in the canvas.

## Steps to Reproduce the Problem

  1. Create Controller
  2. Create another Controller
  3. Press and hold on a Controller in the canvas
  4. Drag the link to the other Controller
  5. The link does not appear between the two elements

## Specifications

  - 16.04 LTS
  - Platform: Linux  
  - Subsystem:

