// Revisione di qualifica sv & cv.
// se un doc non è presente nelle tabelle della sv => non ha subito modifiche nel periodo => non lo calcolo.
// stesso discorso di sopra per cv.
/*
ore di amm disponibili = 6
*/

					~SV~

Voglio misurare sv e cv rispetto al RA
————————————————————————————————————————————————————————————————————————
       DOC         |        end produzione 	|   end verifica 
      
       PDP         |		 17/08		|	 17/08 | 

       AR 	   |		27/07		|	26/07 |

       NP	   |	        01/08		|	31/07 |

       PDQ	   |		20/08	        |	17/08 |

       MS	   |		25/07	        |	29/07 |

       MU	   |		25/07	        |	29/07 |

————————————————————————————————————————————————————————————————————————

GIORNI DISPONIBILI di redazione/verifica
start: 26/07/2018
end:  24/08/2018


					~CV~

PdP (3h)
	1. Preventivo:
		Costo totale=80
		Costo verifica=30
	2. Consuntivo:
		Costo totale=60
		Costo verifica=45
	CV = 60 - 80 = -20
	CV (ver) = 45 -30 = 15

AR (30 min)
	1. Preventivo:
		Costo totale=20
		Costo verifica=30
	2. Consuntivo:
		Costo totale=10
		Costo verifica=8
	CV = 10 - 20 = -10
	CV (ver) = 8 - 30 = 22

NdP (30 min)
	1. Preventivo:
		Costo totale=20
		Costo verifica=30
	2. Consuntivo:
		Costo totale=10
		Costo verifica=7
	CV = 10 - 20 = -10
	CV (ver) = 7 - 30 = 23

PdQ (2h)
	1. Preventivo
		Costo totale=60
		costo verifica=30
	2. Consuntivo:
		Costo totale=40
		costo verifica=45
	CV = 40 - 60 = -20
	CV (ver) = 45 - 30 = 15

MS
	1. Preventivo
		Costo totale=45
		costo verifica=15
	2. Consuntivo:
		Costo totale=30
		costo verifica=30
	CV = 30 - 45 = -15
	CV (ver) = 30 - 15 = 15

MU
	1. Preventivo
		Costo totale=30
		costo verifica=15
	2. Consuntivo:
		Costo totale=30
		costo verifica=30
	CV = 30 - 30 = 0
	CV (ver) = 30 - 15 = 15
